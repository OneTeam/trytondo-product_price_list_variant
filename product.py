# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.pool import Pool, PoolMeta
from trytond.i18n import gettext
from trytond.model import fields
from trytond.modules.product import price_digits
from trytond.pyson import Eval

__all__ = ['Product', 'PriceListLine']


class Product(metaclass=PoolMeta):
    'Product'
    __name__ = 'product.product'

    price_list = fields.One2Many('product.price_list.line', 'product',
                                 'Price List',
                                 states={'readonly': Eval('id', -1) < 0})


class PriceListLine(metaclass=PoolMeta):
    'Price List Line'
    __name__ = 'product.price_list.line'

    product_list_price = fields.Function(
        fields.Numeric("Product List Price", digits=price_digits,
                       states={'invisible': ~Eval('product')},
                       depends=['product']),
        'on_change_with_product_list_price')
    product_cost_price = fields.Function(
        fields.Numeric("Product Cost Price", digits=price_digits,
                       states={'invisible': ~Eval('product')},
                       depends=['product']),
        'on_change_with_product_cost_price')
    product_formula_price = fields.Function(
        fields.Char(
            'Formula product Price',
            states={
                'invisible':~Eval('product')
            },
            depends=['price_list', 'product', 'quantity', 'formula'],
            help="Preview price list for 1 product with formula"),
        'on_change_with_product_formula_price')

    @fields.depends('product')
    def on_change_with_product_list_price(self, name=""):
        return getattr(getattr(self, 'product', None), 'list_price', None)

    @fields.depends('product')
    def on_change_with_product_cost_price(self, name=""):
        return getattr(getattr(self, 'product', None), 'cost_price', None)

    @fields.depends('formula', 'price_list', 'product', 'quantity')
    def on_change_with_product_formula_price(self, name=""):
        PriceList = Pool().get('product.price_list')
        formula_price = ""
        list_price = getattr(getattr(self, 'product', None), 'list_price', 0.0)
        # using PriceList and not self.price_list because None value on non
        # saved records.
        context = PriceList.get_context_formula(
            None, None, self.product, list_price or 0.0, self.quantity or 1,
            None, None)
        try:
            unit_price = self.get_unit_price(**context)
            if not isinstance(unit_price, Decimal):
                formula_price = "ValueError"
            else:
                formula_price = str(unit_price)
        except Exception as exception:
            formula_price = gettext(
                'product_price_list.msg_invalid_formula',
                formula=self.formula, line=self.id,
                exception=exception)
        return formula_price
